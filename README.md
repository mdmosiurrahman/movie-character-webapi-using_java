## Movie Characters API

## Technologies uesed
- .Java SDK 17,IntelliJi IDEA
- PostGreSQL 
- Hibernat
-  Maven
-  Spring Initializr/Spring Boot
- JPA

## Project Description

The assignment was to create a PostGreSQL database using Hibernate and later expose it through a Web API in Spring Web. The content of the assignment was about different movies and characters, as well as different types of franchises and how these interact with each other.You can read about the assignment [Assignment_3_Java_Web_API_creation_with_Hibernate__1_.pdf]).

## Project Structure
The project structure involves three different entities in Movie, Character and Franchise. Hibernate has been used to provide an object-relation-mapping for these entities. The central part in the structure is between three different packages which includes model representation for the PostGreSQL database, controller classes for CRUD operations and lastly repository interfaces. A databaseseeder are following with some dummy data, this can be changed in DatabaseSeeder.class

## Documentation
Swagger API has been included to the project to handle API documentation. Following are also a postman_collection file with calls to the API endpoints. [Swagger URL](http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)
