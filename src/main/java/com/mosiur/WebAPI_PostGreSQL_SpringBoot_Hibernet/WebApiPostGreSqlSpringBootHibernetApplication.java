package com.mosiur.WebAPI_PostGreSQL_SpringBoot_Hibernet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class WebApiPostGreSqlSpringBootHibernetApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebApiPostGreSqlSpringBootHibernetApplication.class, args);
	}

}
