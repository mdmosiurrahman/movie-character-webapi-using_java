package com.mosiur.WebAPI_PostGreSQL_SpringBoot_Hibernet.repositories;

import com.mosiur.WebAPI_PostGreSQL_SpringBoot_Hibernet.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
}


