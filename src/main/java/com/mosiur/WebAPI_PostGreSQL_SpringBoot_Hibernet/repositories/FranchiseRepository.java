package com.mosiur.WebAPI_PostGreSQL_SpringBoot_Hibernet.repositories;

import com.mosiur.WebAPI_PostGreSQL_SpringBoot_Hibernet.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository <Franchise, Integer> {
}
